# helpme-commands

A collection of YAML files to be used by [helpme](https://gitlab.com/tprestegard/helpme).

To do:
* MySQL commands
* Pytest commands
* Tox commands
* Vim
  * NERDTree
  * Regex
  * Reading multiple files at once
* cloud provider rsync
* Running django commands in docker compose
* Set default audio device: pactl list short sources pactl set-default-sink, edit /etc/pulse/default.pa
